# GitReaper

Script to automate cleaning up git repositories of old, no longer required branches.

Also serves as a handy-dandy tool for brushing up on Python.

## Installation

You can easily set up the dependencies for gitreaper using pipenv. If you do not have pipenv, you can install it using pip:

`sudo pip3 install pipenv`

To install the dependencies for gitpython, inside this directory, run:

`pipenv install`

Running gitreaper using pipenv:

<pre>
cd /path/to/sre-scripts/gitpython
pipenv run python gitreaper.py <args>
</pre>

To make an alias for easier execution, add this alias to your .bashrc or profile file of choice:

`alias gitreaper="cd /path/to/sre-scripts/gitreaper && pipenv run python gitreaper.py"`

You can then just type `gitreaper <args>` to clean up your repos any time!

## Usage

`gitreaper [-h] [-r REPO_DIR] [-x EXCLUDE] [-a] [-t] [-l] [-d] [-y] [-v]`

<pre>
Optional Arguments:
  -h, --help            show this help message and exit
  -r REPO_DIR, --repo-dir REPO_DIR
                        Specify the parent directory to search for git
                        repositories  (defaults to ~/dev).
  -x EXCLUDE, --exclude EXCLUDE
                        Add the name of a branch to be excluded from reaping.
  -a, --reap-active     Reap a branch even if it is the active (checked-out)
                        branch.
  -t, --reap-tracked    Reap a local branch even if it is being tracked
                        remotely.
  -l, --save-local      Do not reap local branches that are not being tracked
                        remotely.
  -d, --dry-run         Perform a dry-run of the reaping.
  -y, --yes             Say 'yes' to all questions and do not prompt the user
                        (use with caution).
  -v, --verbose         Enable verbose output.
</pre>

## Changing Defaults

If you want to change the default directory and excluded branches, you can modify the following lines in the script:

<pre>
DEFAULT_REPO_DIR = os.path.join(os.getenv('HOME'), 'dev')
DEFAULT_EXCLUDED_BRANCHES = ['master', 'deploy', 'develop']
</pre>

Changing these will allow you to not need to use `--repo-dir` or `--exclude` command-line args on a per-run basis.