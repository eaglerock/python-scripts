import argparse
import git
import logging
import os
import sys

"""
gitreaper - reap your old git branches away!

Requires Python 3.7 to run, because I'm lazy and f-strings are so much prettier.

Usage: gitreaper [-h] [-r REPO_DIR] [-x EXCLUDE] [-a] [-t] [-l] [-d] [-y] [-v]

Optional Arguments:
  -h, --help            show this help message and exit
  -r REPO_DIR, --repo-dir REPO_DIR
                        Specify the parent directory to search for git
                        repositories (defaults to ~/dev).
  -x EXCLUDE, --exclude EXCLUDE
                        Add the name of a branch to be excluded from reaping.
  -a, --reap-active     Reap a branch even if it is the active (checked-out)
                        branch.
  -t, --reap-tracked    Reap a local branch even if it is being tracked
                        remotely.
  -l, --save-local      Do not reap local branches that are not being tracked
                        remotely.
  -d, --dry-run         Perform a dry-run of the reaping.
  -y, --yes             Say 'yes' to all questions and do not prompt the user
                        (use with caution).
  -v, --verbose         Enable verbose output.

TODOs:
- Allow for deleting remote branches when specified through command-line arg
"""

DEFAULT_REPO_DIR = os.path.join(os.getenv("HOME"), "dev")
DEFAULT_EXCLUDED_BRANCHES = ["master", "deploy", "develop"]


class GitReaper:
    """
    Handles git repos and manages commands
    """

    def __init__(self):
        self._scan_args()
        self._set_logging()
        self.repos = self._scan_repos()

    def _scan_args(self):
        """
        Scans command-line arguments and sets class variables appropriately
        """
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "-r",
            "--repo-dir",
            action="store",
            default=DEFAULT_REPO_DIR,
            help="Specify the parent directory to search for git repositories (defaults to ~/dev).",
        )
        parser.add_argument(
            "-x",
            "--exclude",
            action="append",
            default=DEFAULT_EXCLUDED_BRANCHES,
            help="Add the name of a branch to be excluded from reaping.",
        )
        parser.add_argument(
            "-a",
            "--reap-active",
            action="store_true",
            help="Reap a branch even if it is the active (checked-out) branch.",
        )
        parser.add_argument(
            "-t",
            "--reap-tracked",
            action="store_true",
            help="Reap a local branch even if it is being tracked remotely.",
        )
        parser.add_argument(
            "-l",
            "--save-local",
            action="store_false",
            help="Do not reap local branches that are not being tracked remotely.",
        )
        parser.add_argument(
            "-d",
            "--dry-run",
            action="store_true",
            help="Perform a dry-run of the reaping.",
        )
        parser.add_argument(
            "-y",
            "--yes",
            action="store_true",
            help="Say 'yes' to all questions and do not prompt the user (use with caution).",
        )
        parser.add_argument(
            "-v", "--verbose", action="store_true", help="Enable verbose output."
        )

        args = parser.parse_args()

        self.repo_dir = args.repo_dir
        self.excluded = args.exclude
        self.reap_active = args.reap_active
        self.reap_tracked = args.reap_tracked
        self.save_local = args.save_local
        self.dry_run = args.dry_run
        self.force_yes = args.yes
        self.verbose = args.verbose

    def _set_logging(self):
        """
        Sets up the logger to print to stdout and sets the verbosity based on the command input.
        """
        log = logging.getLogger(__name__)

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(logging.Formatter("%(message)s"))
        stdout_handler.setLevel(logging.INFO)

        log.addHandler(stdout_handler)

        if self.verbose:
            log.setLevel(logging.INFO)
        else:
            log.setLevel(logging.WARNING)

        self.log = log

    def _scan_repos(self):
        """
        Scans the user's REPO_DIR for cloned repos and loads into found_repos
        """
        self.log.info(f"Scanning {self.repo_dir} for valid git repositories:")
        found_repos = []
        found_dirs = os.listdir(self.repo_dir)

        for directory in found_dirs:
            try:
                repo = git.Repo(os.path.join(self.repo_dir, directory))
            except git.InvalidGitRepositoryError:
                self.log.info(f"  Skipping {directory}: not a git repo")
                continue

            self.log.info(f"  Found repository: {directory}")
            found_repos.append(repo)

        return found_repos

    def _skip_excluded_branch(self, branch):
        """
        Checks if the branch in question is an excluded branch to be skipped.

        Params:
            branch - the HEAD object of the branch to be checked

        Returns:
            True  - if the branch is excluded and should be skipped.
            False - if the branch is not excluded and can be reaped.
        """
        if branch.name in self.excluded:
            self.log.info(f"  {branch} is an excluded branch. Skipping.")
            return True
        else:
            return False

    def _skip_active_branch(self, branch, repository):
        """
        Checks if the branch in question is actively checked out.
        Will skip if the branch is active unless --reap-active was selected.

        Params:
            branch     - the HEAD object of the branch to be checked
            repository - the REPO object of the current repository being worked on

        Returns:
            True  - if the branch is active and should be skipped.
            False - if the branch is not active or if it should be reaped anyway.
        """
        if branch == repository.active_branch:
            if self.reap_active:
                if repository.is_dirty():
                    self.log.info(
                        f"  {branch} is the active (checked-out) branch, but it is dirty, so it will be skipped."
                    )
                    self.log.info(
                        "    (To delete this branch, first review and commit/discard your current changes.)"
                    )
                    return True
                else:
                    self.log.info(
                        f"  {branch} is the active (checked-out) branch, but continuing anyway per your command."
                    )
                    if not self.dry_run:
                        repository.heads.master.checkout()
            else:
                self.log.info(
                    f"  {branch} is the active (checked-out) branch, so it will be skipped."
                )
                self.log.info("    (To purge active branches, use the -a switch.)")
                return True
        return False

    def _skip_tracked_branch(self, branch, repository):
        """
        Checks if the branch in question is being tracked remotely.
        Will skip if the branch is tracked remotely unless --reap-tracked was selected.

        Params:
            branch     - the HEAD object of the branch to be checked
            repository - the REPO object of the current repository being worked on

        Returns:
            True  - if the branch is being tracked remotely and should be skipped
            False - if the branch is not being remotely tracked or if it should be reaped anyway
        """
        try:
            remote = repository.remote("origin")
            remote.fetch(branch)
            if self.reap_tracked:
                self.log.info(
                    f"  {branch} is being tracked remotely, but continuing anyway per your command."
                )
                return False
            else:
                self.log.info(
                    f"  {branch} is bring tracked remotely, so it will be skipped."
                )
                self.log.info(
                    "    (To purge local branches that are being tracked remotely, use the -t switch.)"
                )
                return True
        except git.GitCommandError:
            if self.save_local:
                return False
            else:
                self.log.info(
                    f"  {branch} is not tracked remotely, so it will be skipped."
                )
                self.log.info(
                    "    (To purge local branches that are not tracked remotely, omit the -l switch.)"
                )
                return True

    def _ask_user(self, question):
        """
        Asks a question to the user and prompts for user input. Loops if incorrect response received.

        Params:
            question - String of question to be asked to the user

        Returns:
            True  - If answer was 'y'
            False - If answer was 'n'
        """
        answer = ""
        while not answer in ["y", "n"]:
            answer = str(input(f"{question} (Y/N): ")).lower().strip()

            if answer == "y":
                return True
            if answer == "n":
                return False

            print("\nPlease enter 'y' or 'n'.")

    def _purge(self, repository, branch_list):
        """
        Purges the branch_list of git branches to be remoted from repo.

        Params:
            repository  - Git repository to have branches purged from
            branch_list - List of branches pending deletion from the repo
        """
        repo_name = os.path.basename(repository.working_tree_dir)

        if not branch_list:
            self.log.info(f"\n{repo_name} has no branches to delete, skipping.")
            return

        dry_run = " (dry-run)" if self.dry_run else ""

        self.log.warning(f"\nBranches to delete from {repo_name}{dry_run}:")
        for branch in branch_list:
            self.log.warning(f"  {branch}")

        if self.force_yes:
            self.log.info(f"\nAnswering yes since -y was specified.")
        elif not self._ask_user("Is this OK?"):
            self.log.warning(f"Skipping purging branches for {repo_name} as requested.")
            return

        self.log.warning(f"Purging branches from {repo_name}{dry_run}...")
        for branch in branch_list:
            self.log.info(f"  Purging branch: {branch}")
            if not self.dry_run:
                repository.delete_head(branch)

    def git_reap_o(self):
        """
        Runs through the list of repos and reaps old branches from each one
        """
        run = "dry run" if self.dry_run else "reaping"
        self.log.warning(
            f"\nPerforming a {run} of all found repos in {self.repo_dir}..."
        )

        for current_repo in self.repos:
            repo_name = os.path.basename(current_repo.working_tree_dir)

            self.log.info(f"\nStarting the {run} for the {repo_name} repo:")
            reapables = []

            for branch in current_repo.branches:
                if self._skip_excluded_branch(branch):
                    continue

                if self._skip_active_branch(branch, current_repo):
                    continue

                if self._skip_tracked_branch(branch, current_repo):
                    continue

                self.log.info(f"  Adding {branch} to the list of reapable branches.")
                reapables.append(branch)

            self._purge(current_repo, reapables)


def fear_the_reaper():
    my_reaper = GitReaper()
    my_reaper.git_reap_o()


fear_the_reaper()
