# python-scripts

Python scripts I've written to make my life easier.

## scripts

subdirectory | contents
---: | ---
`gitreaper` | Script to automate cleaning up git repositories of old, no longer required branches.

